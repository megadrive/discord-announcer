'use strict'

let conf = require('./config.json')
let Discord = require('discord.js')
let bot = new Discord.Client()
bot.login(conf.discord.key)
const got = require('got')

const cached = {}

/** Whether or not the bot is paused. */
let isPaused = false

function announce (member, streamData, gameData, isRich = false) {
  const presence = member.presence
  const channel = member.guild.channels.get(conf.discord.announceChannel)

  if (isRich) {
    const embed = new Discord.RichEmbed()
      .setThumbnail(gameData.box_art_url.replace(/\{.+?\}/g, '64'))
      .setDescription(`${member.username} has started streaming!`)
      .addField('Title', presence.game.name)
      .addField('Game', gameData.name)
      .addField('Watch', presence.game.url)
    console.log('sent embed')
    channel.send({embed})
  } else {
    const template = '{name} is streaming **{game}** at {url} -- "*{title}*"'
    let say = template
      .replace('{name}', member.user.username)
      .replace('{game}', gameData.name)
      .replace('{title}', presence.game.name)
      .replace('{url}', presence.game.url)
    console.log(say)
    channel.send(say)
  }
}

function getTwitchUsername (url) {
  const rtwitch = /https?:\/\/(www\.)?twitch\.tv\/(.+)/i
  const result = rtwitch.exec(url)

  if (result && result[2]) {
    return result[2]
  }
  return null
}

bot.on('ready', () => {
  console.log('ready')
})

bot.on('presenceUpdate', (oldMember, newMember) => {
  if (oldMember.guild.id !== conf.discord.guildid) return
  if (isPaused) return

  const presence = newMember.presence

  // check that discord hasn't fired this shit off again
  if (cached[newMember.id] && oldMember.presence.equals(cached[newMember.id])) return

  if (presence.game && presence.game.url) {
    twitchApi('streams?user_login=' + getTwitchUsername(presence.game.url))
      .then(streamData => {
        twitchApi('games?id=' + streamData.game_id)
          .then(gameData => {
            announce(oldMember, streamData, gameData, true)
            cached[oldMember.id] = presence
          })
      })
      .catch(console.log)
  }
})

/**
 * check the twitch api for stream information
 */
function twitchApi (endpoint) {
  return new Promise(function (resolve, reject) {
    got(`https://api.twitch.tv/helix/${endpoint}`, {
      headers: {
        'Client-ID': conf.twitch.clientid
      },
      json: true
    }).then(res => {
      if (res.body.data.length) {
        resolve(res.body.data[0])
      }
    })
  })
}

// DEBUG
bot.on('message', (message) => {
  if (message.guild.id !== conf.discord.guildid) return
  if (message.author.id !== conf.discord.adminid) return

  if (message.content.startsWith('!mute') ||
      message.content.startsWith('!pause')) {
    isPaused = !isPaused

    message.reply(`AnnouncerBot will now ${isPaused ? 'not' : ''} announce.`)
  }

  if (message.content.startsWith('!test')) {
    let testUserOld = bot.fetchUser(conf.discord.adminid)
    testUserOld.guild = bot.guilds.get(conf.discord.guildid)
    const testUserNew = testUserOld
    testUserNew.presence = {}
    testUserNew.presence.game = {
      name: 'Test stream title',
      type: 1,
      url: 'https://www.twitch.tv/criken'
    }
    bot.emit('presenceUpdate', testUserOld, testUserNew)
  }
})

bot.on('warn', console.warn)
